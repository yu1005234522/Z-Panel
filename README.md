# Z-Panel

#### 介绍

基于Layui2.5.5、ThinkPHP6.0开发的后台管理面板+OA系统基础班，以企业级应用为目标，可通过TP的分布式数据库管理多个前台系统。

面板基于TP6的多应用模式下开发，与单应用略有不同。

默认用户名：z@dqzhiyu.com

默认密码：dqzhiyu.com

#### 特点

1.  PC、移动端自适应
2.  模块化三级权限

#### 应用场景

1.  后台面板
2.  OA系统
3.  项目进度管理
4.  数据分析

#### 安装教程

1.  修改.example.env文件，配置数据库并另存为.env文件
2.  导入zpanel.sql
3.  设置public为访问目录

#### 已完成功能

1.  登录
2.  三级自定义菜单
3.  自定义全站设置项
4.  自定义用户组
5.  自定义用户资料项
6.  头像上传与安全设置
7.  资料编辑
8.  用户管理

#### 未完成功能

1.  操作日志
2.  登录校验
3.  插件实现

#### 文档参考

1.  系统Wiki：[https://gitee.com/crystar/Z-Panel/wikis](https://gitee.com/crystar/Z-Panel/wikis)
2.  Layui：[https://www.layui.com/](https://www.layui.com/)
3.  ThinkPHP：[https://www.kancloud.cn/manual/thinkphp6_0/](https://www.kancloud.cn/manual/thinkphp6_0/)

#### 附录

1.  项目源自自己在重构自己企业的系统时，发现没有很好的后台管理模式，因此便决定打造这套可用于多系统的模块化面板，同时也将作为自己公司的OA系统。目前项目还在逐步开发中。
2.  关于用户表到底是user还是admin，这个问题我思考了好久，前后改了四次，最后决定还是用user，因为这套系统的定位是后台+OA系统，如果应用到OA系统，显然user表更合适。如果和系统本身的user表冲突，我建议是前后台分离为两套程序，通过TP的分布式数据库进行管理。