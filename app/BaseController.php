<?php

declare (strict_types = 1);

namespace app;

use think\App;
use think\exception\ValidateException;
use think\Facade\View;
use think\Validate;

/**
 * 控制器基础类
 */
abstract class BaseController {
	/**
	 * Request实例
	 * @var \think\Request
	 */
	protected $request;

	/**
	 * 应用实例
	 * @var \think\App
	 */
	protected $app;

	/**
	 * 是否批量验证
	 * @var bool
	 */
	protected $batchValidate = false;

	/**
	 * 控制器中间件
	 * @var array
	 */
	protected $middleware = [];

	/**
	 * 用户
	 * @var [type]
	 */
	protected $user;

	/**
	 * 用户ID
	 * @var [type]
	 */
	protected $user_id;

	/**
	 * 系统全局设置
	 * @var [type]
	 */
	protected $settings;

	/**
	 * 用户组
	 * @var [type]
	 */
	protected $group;

	/**
	 * 构造方法
	 * @access public
	 * @param  App  $app  应用对象
	 */
	public function __construct(App $app) {
		$this->app = $app;
		$this->request = $this->app->request;
		// 控制器初始化
		$this->initialize();
	}

	// 初始化
	protected function initialize() {
		$app = strtolower(app('http')->getName());
		$controller = $this->request->controller(true);
		$action = $this->request->action(true);
		$this->user = $this->request->user;
		$this->user_id = $this->user['user_id'];
		$this->group = $this->request->group;
		$this->settings = $this->request->settings;
		View::assign('modApp', $app);
		View::assign('modController', $controller);
		View::assign('modAction', $action);
	}

	/**
	 * 验证数据
	 * @access protected
	 * @param  array        $data     数据
	 * @param  string|array $validate 验证器名或者验证规则数组
	 * @param  array        $message  提示信息
	 * @param  bool         $batch    是否批量验证
	 * @return array|string|true
	 * @throws ValidateException
	 */
	protected function validate(array $data, $validate, array $message = [], bool $batch = false) {
		if (is_array($validate)) {
			$v = new Validate();
			$v->rule($validate);
		} else {
			if (strpos($validate, '.')) {
				// 支持场景
				[$validate, $scene] = explode('.', $validate);
			}
			$class = false !== strpos($validate, '\\') ? $validate : $this->app->parseClass('validate', $validate);
			$v = new $class();
			if (!empty($scene)) {
				$v->scene($scene);
			}
		}

		$v->message($message);

		// 是否批量验证
		if ($batch || $this->batchValidate) {
			$v->batch(true);
		}

		return $v->failException(true)->check($data);
	}

	/**
	 * 设置网页title
	 * @DateTime 2019-12-23
	 * @Author   BadmintonCode
	 * @param    string        $title 标题
	 * @return   [type]               [description]
	 */
	protected function title($title) {
		View::assign('title', $title);
		return $this;
	}

	/**
	 * 无需加载默认的模块化js文件
	 * @DateTime 2019-12-23
	 * @Author   BadmintonCode
	 * @return   [type]               [description]
	 */
	protected function noJs() {
		View::assign('noJs', true);
		return $this;
	}

	/**
	 * 增加js文件
	 * @DateTime 2019-12-23
	 * @Author   BadmintonCode
	 * @param    string        $root 填写js绝对路径
	 * @return   [type]              [description]
	 */
	protected function js($root) {
		if (!is_array($root)) {
			$root = [$root];
		}

		View::assign('js', $root);
		return $this;
	}

	/**
	 * 增加css文件
	 * @DateTime 2019-12-23
	 * @Author   BadmintonCode
	 * @param    string        $root 填写css绝对路径
	 * @return   [type]              [description]
	 */
	protected function css($root) {
		if (!is_array($root)) {
			$root = [$root];
		}

		View::assign('css', $root);
		return $this;
	}

	/**
	 * 设置模块化js的应用名
	 * @DateTime 2019-12-23
	 * @Author   BadmintonCode
	 * @param    string        $app 名称
	 * @return   [type]             [description]
	 */
	protected function app($app) {
		View::assign('modApp', $app);
		return $this;
	}

	/**
	 * 设置模块化js的控制器名
	 * @DateTime 2019-12-23
	 * @Author   BadmintonCode
	 * @param    string        $controller 名称
	 * @return   [type]                    [description]
	 */
	protected function controller($controller) {
		View::assign('modController', $controller);
		return $this;
	}

	/**
	 * 设置模块化js的方法名
	 * @DateTime 2019-12-23
	 * @Author   BadmintonCode
	 * @param    string        $action 名称
	 * @return   [type]                [description]
	 */
	protected function action($action) {
		View::assign('modAction', $action);
		return $this;
	}

}
