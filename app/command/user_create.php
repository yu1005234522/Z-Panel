<?php
declare (strict_types = 1);

namespace app\command;

use app\common\model\User as UserModel;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\Facade\Lang;

class user_create extends Command {
	protected function configure() {
		// 指令配置
		$this->setName('user:create')
			->addArgument('email', Argument::OPTIONAL, "E-mail")
			->addArgument('password', Argument::OPTIONAL, "Password")
			->addOption('group_id', null, Option::VALUE_OPTIONAL, 'Group id.Super Group:1')
			->addOption('status', null, Option::VALUE_OPTIONAL, 'User status.1 for availabled,0 for disabled.Default value:1')
			->setDescription('Create a new user');
	}

	protected function execute(Input $input, Output $output) {
		$group_id = $input->getOption('group_id') === null ? 1 : $input->getOption('group_id');
		if ($group_id != 1) {
			$group_id = 0;
		}

		$status = $input->getOption('status') === null ? 1 : $input->getOption('status');
		if ($status != 1) {
			$status = 0;
		}
		$user = UserModel::register($input->getArgument('email'), $input->getArgument('password'), $status, $group_id);
		if (!is_string($user)) {
			// 指令输出
			$output->writeln('Register succeed');
			$output->writeln('ID:' . $user->user_id);
		} else {
			$output->writeln('Register failed');
			$output->writeln('Error:' . Lang::get($user));
		}
	}
}
