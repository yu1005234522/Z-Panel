<?php
declare (strict_types = 1);

namespace app\command;

use app\common\model\User as UserModel;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\Facade\Lang;

class user_password extends Command {
	protected function configure() {
		// 指令配置
		$this->setName('user:password')
			->addArgument('user_id', Argument::OPTIONAL, "User id")
			->addArgument('password', Argument::OPTIONAL, "Password")
			->setDescription('Change user\'s password');
	}

	protected function execute(Input $input, Output $output) {

		$user = UserModel::password($input->getArgument('user_id'), $input->getArgument('password'));
		if (!is_string($user)) {
			// 指令输出
			$output->writeln('Password changed');
		} else {
			$output->writeln('Changing failed');
			$output->writeln('Error:' . Lang::get($user));
		}
	}
}
