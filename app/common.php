<?php
// 应用公共文件
define('VERSION', '1.00.191220A');
define('FOOTER', 'Z-Panel &copy; 2020 <a href="http://www.dqzhiyu.com" title="浙江德清智羽网络技术有限公司" target="_blank">dqzhiyu.com</a>');
use think\Facade\Lang;
use think\Facade\Request;
use think\Facade\View;
/**
 * 生成带salt的密码
 * @DateTime 2019-12-13
 * @Author   BadmintonCode
 * @param    string        $salt     16位salt
 * @param    string        $password 密码
 * @return   string                  摘要后的密码
 */
function salt_password($salt, $password) {
	return md5(md5($password) . $salt);
}
/**
 * 返回成功状态
 * @DateTime 2019-12-14
 * @Author   BadmintonCode
 * @param    string        $msg    若类型为string，则为msg，若为array，则为data，若为bool，则为token
 * @param    array         $data   [description]
 * @param    boolean       $token  [description]
 * @param    array         $params [description]
 * @return   [type]                [description]
 */
function doSuccess($msg = '', $data = [], $token = false, $params = []) {
	return doReturn($msg, $data, $token, $params, 200);
}
/**
 * 返回状态
 * @DateTime 2019-12-14
 * @Author   BadmintonCode
 * @param    string        $msg    若类型为string，则为msg，若为array，则为data，若为bool，则为token
 * @param    array         $data   [description]
 * @param    boolean       $token  true则携带新的token
 * @param    array         $params msg传入参数
 * @param    integer       $code   [description]
 * @return   [type]                [description]
 */
function doReturn($msg = '', $data = [], $token = false, $params = [], $code = 201) {
	if (is_bool($msg)) {
		$token = $msg;
		$msg = '';
	} elseif (is_bool($data)) {
		$token = $data;
		$data = [];
	}
	if (is_array($msg)) {
		$data = $msg;
		$msg = '';
	}
	$return = ['code' => $code];
	if ($token) {
		$return['token'] = request()->buildToken('__token__');
	}
	if ($msg) {
		$return['msg'] = Lang::get($msg, $params);
	}

	$return['data'] = $data;

	return json($return);
}
/**
 * 成功提示页
 * @DateTime 2019-12-16
 * @Author   BadmintonCode
 * @param    string        $msg   提示文字
 * @param    string        $url   跳转链接
 * @param    string        $title 标题
 * @param    integer       $time  跳转时间
 */
function success($msg, $url = '', $title = '操作成功', $time = 3) {
	return notice($msg, $url, 'face-smile', $title, $time);
}
/**
 * 错误提示页
 * @DateTime 2019-12-16
 * @Author   BadmintonCode
 * @param    string        $msg   提示文字
 * @param    string        $url   跳转链接
 * @param    string        $title 标题
 * @param    integer       $time  跳转时间
 */
function error($msg, $url = '', $title = '操作失败', $time = 3) {
	return notice($msg, $url, 'face-surprised', $title, $time);
}
/**
 * 提示信息页，若为ajax则执行doReturn
 * @DateTime 2019-12-16
 * @Author   BadmintonCode
 * @param    string        $msg   提示文字
 * @param    string        $url   跳转链接
 * @param    string        $icon  icon样式
 * @param    string        $title 标题
 * @param    integer       $time  跳转时间
 */
function notice($msg, $url = '', $icon = 'tips', $title = '系统提示', $time = 3) {
	if (Request::isAjax()) {
		return doReturn($msg, ['url' => $url], false, [], $title == '操作成功' ? 200 : 201);
	} else {
		$view = View::fetch('common@index/notice', ['noJs' => true, 'title' => $title, 'icon' => $icon, 'msg' => $msg, 'time' => $time, 'url' => $url]);
		//echo $view;
		return $view;
	}
}