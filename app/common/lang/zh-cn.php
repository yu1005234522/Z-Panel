<?php
return [
	'email' => [
		'empty' => '请输入邮箱',
		'format' => '邮箱格式不正确',
		'exist' => '邮箱已存在',
	],
	'password' => [
		'empty' => '请输入密码',
		'short' => '密码不得少于6位',
	],
	'user_id' => [
		'notExist' => 'ID不存在',
		'empty' => '请输入ID',
	],
	'systemDelete' => '系统项无法删除',
	'token' => 'Token校验失败',

	'success' => [
		'save' => '保存成功',
		'modify' => '修改成功',
		'add' => '添加成功',
		'delete' => '删除成功',
	],
	'group' => [
		'notExist' => '用户组不存在',
	],
	'status' => ['error' => '状态不正确'],
	'user' => [
		'username' => '请输入邮箱或工号',
		'notExist' => '用户不存在',
		'error' => '密码错误',
		'success' => '登录成功',
		'baned' => '您无权登录面板',
		'confirm' => '两次密码不一致',
		'fieldReadonly' => '字段不可修改',
		'nopower' => '出于安全考虑无法直接操作%s，可至数据库中修改或配置环境变量',
	],
];
?>