<?php
namespace app\common\model;

use think\Model;

class Menu extends Model {
	protected $pk = 'menu_id';
	/**
	 * 获取菜单
	 * @DateTime 2019-12-15
	 * @Author   BadmintonCode
	 * @param    integer       $menu_id   父菜单id
	 * @param    boolean       $all       是否递归获取所有菜单
	 * @param    integer       $available 1为仅获取可用，0为获取所有
	 * @return   [type]                   [description]
	 */
	public function getMenus($menu_id = 0, $all = true, $available = 1) {
		$model = self::whereParentId($menu_id);
		if ($available) {
			$model->whereAvailable(1);
		}
		$model->order('displayorder', 'asc');
		$menus = $model->select();
		if ($all) {
			foreach ($menus as $key => $value) {
				if ($menus[$key]['level'] >= 3) {
					break;
				}

				$childs = self::getMenus($value->menu_id, $all, $available);
				$menus[$key]['children'] = $childs;

			}
		}
		return $menus;
	}
	public function getPathAttr($value, $data) {
		if ($data['url']) {
			return $data['url'];
		}

		return url('/' . $data['app'] . '/' . $data['controller'] . '/' . $data['action']);
	}

}