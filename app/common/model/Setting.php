<?php
namespace app\common\model;

use think\facade\Cache;
use think\Model;

class Setting extends Model {

	public function getAll() {
		$settings = self::order('setting_id', 'asc')->select()->toArray();

		$list = [];
		foreach ($settings as $key => $value) {
			if (in_array($value['type'], ['select', 'multiSelect'])) {
				$list[$value['setkey']] = explode(',', $value['setvalue']);
			} else {
				$list[$value['setkey']] = $value['setvalue'];
			}
		}
		Cache::set('settings', $list);

		return $settings;
	}
	public function getSetvalueAttr($data) {
		return stripslashes($data);
	}
}