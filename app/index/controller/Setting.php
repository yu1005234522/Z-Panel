<?php
declare (strict_types = 1);

namespace app\index\controller;
use app\BaseController;
use app\common\model\Menu as MenuModel;
use think\Facade\Db;
use think\Facade\View;

class Setting extends BaseController {
	/**
	 * 菜单设置
	 * @DateTime 2019-12-15
	 * @Author   BadmintonCode
	 */
	public function menu() {

		if ($this->request->isAjax()) {
			if ($this->request->isPost()) {
				//更新菜单
				$menus = [];
				$menu_ids = [];
				function getMenu($data, $level = 1) {
					global $menu_ids;
					$menus = [];
					if ($data) {
						foreach ($data as $key => $value) {
							$menu = [
								'menu_id' => trim(strip_tags($value['menu_id'])),
								'title' => trim(strip_tags($value['title'])),
								'level' => $level,
								'type' => intval($value['type']),
								'available' => intval($value['available']) == 1 ? 1 : 0,
								'is_system' => intval($value['is_system']) == 1 ? 1 : 0,
								'url' => trim(strip_tags((string) $value['url'])),
								'displayorder' => $key,
								'app' => strtolower(trim(strip_tags((string) $value['app']))),
								'controller' => strtolower(trim(strip_tags((string) $value['controller']))),
								'action' => strtolower(trim(strip_tags((string) $value['action']))),
							];
							if ($menu['menu_id'] != 'new') {
								$menu_ids[] = $menu['menu_id'];
							}
							if ($level <= 2) {
								$menu['children'] = getMenu($value['children'], $level + 1);
							}
							$menus[] = $menu;
						}
					}
					return $menus;
				}
				MenuModel::destroy($menu_ids);
				$menus = getMenu($this->request->post('data'), 1);

				//Db::execute('TRUNCATE TABLE menu');
				function updateMenu($data, $parent_id = 0) {
					if ($data) {
						foreach ($data as $key => $value) {
							$value['parent_id'] = $parent_id;
							if ($value['menu_id'] == 'new') {
								unset($value['menu_id']);
							}
							$insert = $value;
							unset($insert['children']);
							if ($insert['menu_id']) {
								$menu = MenuModel::update($insert);
							} else {
								$menu = new MenuModel;
								$menu->save($insert);
							}
							//$menu_id = Db::name('menu')->insertGetId($insert);
							updateMenu($value['children'], $menu->menu_id);
						}
					}
				}
				updateMenu($menus);
				return doSuccess('success.save', MenuModel::getMenus(0, true, 0)->toArray());
			}
			return doSuccess(MenuModel::getMenus(0, true, 0)->toArray());
		}
		$this->title('菜单设置');
		return View::fetch();
	}
	/**
	 * 全站设置
	 * @DateTime 2019-12-16
	 * @Author   BadmintonCode
	 */
	public function site() {
		if ($this->request->isAjax()) {
			if ($this->request->isPost()) {
				$settings = [];
				$is_system = $this->request->post('is_system');
				$setkey = $this->request->post('setkey');
				$setvalue = $this->request->post('setvalue');
				$setting = $this->request->post('setting');
				$title = $this->request->post('title');
				$type = $this->request->post('type');
				foreach ($is_system as $key => $value) {
					$item = [
						'is_system' => intval($value) == 1 ? 1 : 0,
						'setkey' => trim(strip_tags((string) $setkey[$key])),
						'setvalue' => addslashes((string) $setvalue[$key]),
						'setting' => trim(strip_tags((string) $setting[$key])),
						'title' => trim(strip_tags((string) $title[$key])),
						'type' => trim(strip_tags((string) $type[$key])),
					];
					if (!$item['title']) {
						return error('请输入' . $item['setkey'] . '的标题');
					}
					if (!$item['setkey']) {
						return error('请输入' . $item['title'] . '的标识');
					}
					if (!in_array($item['type'], ['text', 'number', 'password', 'textarea', 'switch', 'select', 'multiSelect'])) {
						return error($item['title'] . '类型选择不正确');
					}
					$settings[] = $item;
				}
				Db::execute('TRUNCATE TABLE setting');
				$model = new \app\common\model\Setting;
				$model->saveAll($settings);
				$settings = $model->getAll();
				return doSuccess('success.save', $settings);
			} else {
				$settings = \app\common\model\Setting::getAll();
				return doSuccess($settings);
			}
		}
		$this->title('全站设置');
		$this->js('/static/libs/xm-select.js');
		return View::fetch();
	}
}
