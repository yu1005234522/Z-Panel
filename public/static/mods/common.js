layui.define(['form', 'element', 'code'], function(exports) {
    var form = layui.form,
        $ = layui.jquery,
        layer = layui.layer,
        element = layui.element;
    layui.code();
    $('.layui-code').find('a[href="http://www.layui.com/doc/modules/code.html"][target="_blank"]').attr('href', "javascript:void(0);").attr('target', '_self').addClass('cpy').html('复制代码');
    var clipboard = new ClipboardJS('.cpy', {
        text: function(trigger) {
            var lis = $(trigger.parentNode.parentNode).children('ol').children('li');
            var code = [];
            lis.each(function() {
                code.push(this.innerHTML);
            })
            return code.join('\n');
        }
    });
    clipboard.on('success', function(e) {
        layer.alert('复制成功');
        e.clearSelection();
    });
    var zadmin = {
        showReturn: function(res, successFunc = '', failFunc = '') {
            var that = this;

            function rt() {
                if (res.code == 200 && successFunc) {
                    if (typeof successFunc == 'function') {
                        successFunc(res);
                    } else {
                        eval('successFunc(res)');
                    }
                } else if (res.code != 200 && failFunc) {
                    if (typeof failFunc == 'function') {
                        failFunc(res);
                    } else {
                        eval('failFunc(res)');
                    }
                } else
                    that.doReturn(res);
            }
            if (res.msg) {
                if (res.data && res.data.alert) {
                    layer.alert(res.msg, {
                        icon: res.code == 200 ? 1 : 2,
                        success: function() {
                            rt();
                        }
                    });
                } else {
                    layer.msg(res.msg, {
                        time: res.data && res.data.time ? res.time : 2000,
                        icon: res.code == 200 ? 1 : 2,
                        success: function() {
                            rt();
                        }
                    });
                }
            } else {
                rt();
            }
        },
        doReturn: function(res) {
            if (!res.data) return;
            if (res.data.url) {
                setTimeout(function() {
                    window.location.href = res.data.url;
                }, 2000)
            } else if (res.data.reload) {
                setTimeout(function() {
                    window.location.reload();
                }, 2000)
            } else if (res.data.pReload) {
                setTimeout(function() {
                    window.parent.location.reload();
                }, 2000)
            }
        },
        loading: function() {
            var loading = layer.load(1, {
                shade: [0.2, '#fff']
            });
            return loading;
        },
        form: {
            row: function(title, name, type = 'text', value = '', require = false, extra = {}, aux = '', placeholder = '') {
                var code = '<div class="layui-form-item">';
                code += '<label class="layui-form-label">' + (require ? '<span class="rq">*</span>' : '') + title + '</label>';
                code += '<div class="layui-input-inline">';
                if (['number', 'text', 'password'].indexOf(type) >= 0) {
                    code += this.input(name, type, value, placeholder, require);
                } else if (type == 'select') {
                    code += this.select(name, value, require, extra);
                }
                code += '</div>';
                if (aux.length > 0) {
                    code += '<div class="layui-form-mid layui-word-aux">' + aux + '</div>';
                }
                code += '</div>';
                return code;
            },
            input: function(name, type, value, placeholder, require) {
                return '<input class="layui-input" autocomplete="off" name="' + name + '" value="' + value + '" placeholder="' + placeholder + '" type="' + type + '" ' + (require ? 'required lay-verify="required"' : '') + '>';
            },
            select: function(name, value, require, extra) {
                var code = '<select name="' + name + '" ' + (require ? 'required lay-verify="required"' : '') + '>';
                for (var x in extra) {
                    code += '<option value="' + x + '" ' + (x == value && value.length > 0 ? 'selected=""' : '') + '>' + extra[x] + '</option>';
                }

                code += '</select>';
                return code;
            }
        }
    }

    form.on('submit(*)', function(data) {
        var action = $(data.form).attr('action'),
            button = $(data.elem);

        var loading = layer.load(1, {
            shade: [0.2, '#fff']
        });
        $.post(action, data.field, function(res) {
            layer.close(loading);
            if (res.token) {
                $("input[name='__token__']").val(res.token);
            }
            console.log(action, data.field, res);
            zadmin.showReturn(res);
        })
        return false;
    });
    if (layui.cache.app && layui.cache.controller && layui.cache.action) {
        var extend = {};
        extend[layui.cache.action] = layui.cache.app + '/' + layui.cache.controller + '/' + layui.cache.action;
        layui.extend(extend);
        layui.use(layui.cache.action);
    }
    exports('zadmin', zadmin)
});