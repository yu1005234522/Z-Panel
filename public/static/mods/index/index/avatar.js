layui.define(['zadmin', 'layer','upload'], function(exports) {
    var $ = layui.jquery,
        zadmin = layui.zadmin,
        upload = layui.upload,
        loadingIndex;
    var uploadInst = upload.render({
        elem: '#uploadBtn',
        url:window.location.href,
        before: function(obj) {
            loadingIndex = zadmin.loading();
        },
        done: function(res) {
            layer.close(loadingIndex);
            //如果上传失败
            if (res.code !=200) {
                return layer.msg('上传失败');
            }else{
                layer.msg('上传成功');
                $('#avatar').attr('src', res.data.src);
            }
            //上传成功
        },
        error: function() {
            //演示失败状态，并实现重传
            var demoText = $('#demoText');
            demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
            demoText.find('.demo-reload').on('click', function() {
                uploadInst.upload();
            });
        }
    });
    exports('avatar', {})
})