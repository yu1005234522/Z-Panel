layui.define(['form', 'zadmin'], function(exports) {
    var form = layui.form,
        $ = layui.jquery,
        zadmin = layui.zadmin,
        device = layui.device(),
        element = layui.element;
    //阻止IE7以下访问
    if (device.ie && device.ie < 8) {
        layer.alert('如果您非得使用 IE 浏览器访问Z-Panel，那么请使用 IE8+');
    }
    $("a[data-href]").click(function() {
        var layid = this.dataset.href.replace(/\//g, '_');
        if ($("li[lay-id='" + layid + "']").length == 0) {
            element.tabAdd('iframeTab', {
                title: this.innerText,
                id: layid,
                content: '<iframe src="' + this.dataset.href + '"/>'
            });
        } else {
            var lis = $('li[lay-id]');
            for (var i = 0; i < lis.length; i++) {
                if (lis[i].getAttribute('lay-id') == layid) {
                    var contents = $('#iframeContent').find('iframe');
                    contents[i + 1].contentWindow.location.reload();
                    break;
                }
            }
        }

        element.tabChange('iframeTab', layid);
        //loadIframe();
    })
    $("a[data-menu]").click(function() {
        $('#mobileNav').html(this.innerText);
        $('[lay-filter="leftNav"]').find('li[data-parent!="' + this.dataset.menu + '"]').hide();
        $('[lay-filter="leftNav"]').find('li[data-parent="' + this.dataset.menu + '"]').show();
    })
    $('#showMenu').click(function() {
        $(this).addClass('hide');
        $('#hideMenu').removeClass('hide');
        $('.layui-side').removeClass('left-200').addClass('left_0');
        $('.layui-body').removeClass('left_0').addClass('left_200');
    })
    $('#hideMenu').click(function() {
        $(this).addClass('hide');
        $('#showMenu').removeClass('hide');
        $('.layui-body').removeClass('left_200').addClass('left_0');
        $('.layui-side').removeClass('left_0').addClass('left-200');
    })
    $('#openNew').click(function() {
        var iframe = $(this.parentNode).children('.layui-this');
        if (iframe.attr('lay-id')) {
            window.open(iframe.attr('lay-id').replace(/_/g, '/'));
        }
    })
    $('#refresh').click(function() {
        var layid = $(this.parentNode).children('.layui-this').attr('lay-id');
        var lis = $('li[lay-id]');
        for (var i = 0; i < lis.length; i++) {
            if (lis[i].getAttribute('lay-id') == layid) {
                var contents = $('#iframeContent').find('iframe');
                contents[i + 1].contentWindow.location.reload();
                break;
            }
        }
    })
    element.on('tabDelete(iframeTab)', function(data) {
        var iframes = $('#iframeContent').children('.layui-tab-item');
        $(iframes[data.index-4]).remove();
        if($('#iframeContent').children('.layui-show').length==0){
            var ifi;
            if(iframes.length-1==data.index-4){
                //如果删掉元素是最后一个，则显示前一个
                ifi = data.index-5;
            }else{
                //如果删掉元素是中间的，则显示下一个
                ifi = data.index-3;
            }
            $(iframes[ifi]).addClass('layui-show');
        }
    });
    //手机页面加载
    var treeMobile = $('.site-tree-mobile'),
        shadeMobile = $('.site-mobile-shade')
    treeMobile.on('click', function() {
        $('body').addClass('site-mobile');
    });
    shadeMobile.on('click', function() {
        $('body').removeClass('site-mobile');
    });

    exports('index', {})
})