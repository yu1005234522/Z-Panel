layui.define(['form', 'zadmin', 'layer','laydate'], function(exports) {
    var form = layui.form,
        $ = layui.jquery,
        zadmin = layui.zadmin,
        laydate = layui.laydate;
    $('.z-date').each(function(){
        laydate.render({
            elem: '#'+this.id
            ,type: 'date'
        });
    })
    exports('info', {})
})