layui.define(['form', 'zadmin', 'element'], function(exports) {
    var form = layui.form,
        $ = layui.jquery,
        zadmin = layui.zadmin,
        element = layui.element,
        treeData = [],
        newNode = { menu_id:'new',title: '新菜单', available: 1, type: 0, url: '', app: '', controller: '', action: '', is_system: 0, children: [] };
    var loadingIndex = zadmin.loading();
    $.get(window.location.href, {}, function(res) {
        layer.close(loadingIndex);
        treeData = res.data;
        makeNav();
    })
    $('#addParent').click(function() {
        treeData.push(JSON.parse(JSON.stringify(newNode)))
        $('#save').removeClass('layui-btn-disabled').attr('disabled', false);
        makeNav();
    })
    $('#save').click(function() {
        var loadingIndex = zadmin.loading();
        $.post(window.location.href, { data: treeData }, function(res) {
            layer.close(loadingIndex);
            zadmin.showReturn(res, function(res) {
                treeData = res.data;
                makeNav();
            })
        })
    })

    function makeNav() {
        var nav = $('#navDemo');
        var code = '';
        for (var i = 0; i < treeData.length; i++) {
            code += makeItem(treeData[i], i + 1);
        }
        nav.html(code);
        $('[data-op]').click(function() {
            var elem = this.parentNode.parentNode.parentNode;
            var type = this.dataset.op;
            if (type == 'edit') {
                var item = treeData[elem.dataset.level_1 - 1];
                $('[name="level_1"]').val(elem.dataset.level_1);
                var level = 1;
                if (elem.dataset.level_2 > 0) {
                    item = item.children[elem.dataset.level_2 - 1];
                    $('[name="level_2"]').val(elem.dataset.level_2);
                    level = 2;
                } else {
                    $('[name="level_2"]').val(0);
                }
                if (elem.dataset.level_3 > 0) {
                    item = item.children[elem.dataset.level_3 - 1];
                    $('[name="level_3"]').val(elem.dataset.level_3);
                    level = 3;
                } else {
                    $('[name="level_3"]').val(0);
                }
                $('#level').val(level + '级菜单');
                $('[name="title"]').val(item.title);
                $('#available_' + item.available).attr("checked", true);
                $('#available_' + (1 - item.available)).attr("checked", false);

                $('[name="type"]').val(item.type).attr('disabled', item.is_system == 1);
                $('[name="app"]').val(item.app).attr('disabled', item.is_system == 1);
                $('[name="controller"]').val(item.controller).attr('disabled', item.is_system == 1);
                $('[name="action"]').val(item.action).attr('disabled', item.is_system == 1);
                $('[name="url"]').val(item.url).attr('disabled', item.is_system == 1);

                $('#navForm').show();
                form.render();
                return;
            }

            function doOp(nodes, index) {
                if (type == 'add') {
                    nodes[index].children.push(JSON.parse(JSON.stringify(newNode)));
                } else if (type == 'del') {
                    layer.confirm('您确定要删除【' + nodes[index].title + '】吗？', { icon: 3, title: '操作确认' }, function(layerindex) {
                        nodes.splice(index, 1);
                        makeNav();
                        layer.close(layerindex);
                    });
                } else if (type == 'up') {
                    if (index == 0) return false;
                    var tmp = nodes[index];
                    nodes[index] = nodes[index - 1];
                    nodes[index - 1] = tmp;
                } else if (type == 'down') {
                    if (index + 1 >= nodes.length) return false;
                    var tmp = nodes[index];
                    nodes[index] = nodes[index + 1];
                    nodes[index + 1] = tmp;
                }
                $('#save').removeClass('layui-btn-disabled').attr('disabled', false);
            }

            if (elem.dataset.level_3 > 0 && elem.dataset.level_2 > 0) {
                res = doOp(treeData[elem.dataset.level_1 - 1].children[elem.dataset.level_2 - 1].children, elem.dataset.level_3 - 1);
            } else if (elem.dataset.level_2 > 0) {
                doOp(treeData[elem.dataset.level_1 - 1].children, elem.dataset.level_2 - 1);
            } else {
                doOp(treeData, elem.dataset.level_1 - 1);
            }
            makeNav();
        })
    }

    function makeItem(item, level_1 = 0, level_2 = 0, level_3 = 0) {
        var code = '<div data-level_1="' + level_1 + '" data-level_2="' + level_2 + '" data-level_3="' + level_3 + '" class="layui-tree-set">';
        code += '<div class="layui-tree-entry"><div class="layui-tree-main"><span class="layui-tree-iconClick" data-op="edit"><i class="layui-icon layui-icon-' + (item.is_system == 1 ? 'set-sm' : 'layer') + '"></i></span><span class="layui-tree-txt" data-op="edit">' + item.title + (item.available == 1 ? '[显示]' : '[隐藏]') + '</span></div>';
        code += '<div class="layui-btn-group layui-tree-btnGroup">' + (!level_3 ? '<i class="layui-icon layui-icon-add-1" data-op="add"></i>' : '') + (item.is_system == 1 ? '' : '<i class="layui-icon layui-icon-delete" data-op="del"></i>') + '<i class="layui-icon layui-icon-up" data-op="up"></i><i class="layui-icon layui-icon-down" data-op="down"></i></div>';
        code += '</div>';
        if (item.children && item.children.length > 0) {
            code += '<div class="layui-tree-pack layui-tree-lineExtend layui-tree-showLine" style="display:block;">'
            for (var i = 0; i < item.children.length; i++) {
                if (level_2) {
                    code += makeItem(item.children[i], level_1, level_2, i + 1);
                } else {
                    code += makeItem(item.children[i], level_1, i + 1);
                }
            }
            code += '</div>';
        }
        code += '</div>';
        return code;
    }

    form.on('submit(saveNode)', function(data) {
        var field = data.field;
        var newNode = {
            title: field.title,
            available: field.available,
            type: field.type,
            app: field.app,
            controller: field.controller,
            action: field.action,
            url: field.url,
        };
        if (field.level_3 > 0) {
            newNode.menu_id = treeData[field.level_1 - 1].children[field.level_2 - 1].children[field.level_3 - 1].menu_id;
            newNode.is_system = treeData[field.level_1 - 1].children[field.level_2 - 1].children[field.level_3 - 1].is_system;
            treeData[field.level_1 - 1].children[field.level_2 - 1].children[field.level_3 - 1] = newNode;
        } else if (field.level_2 > 0) {
            newNode.menu_id = treeData[field.level_1 - 1].children[field.level_2 - 1].menu_id;
            newNode.is_system = treeData[field.level_1 - 1].children[field.level_2 - 1].is_system;
            newNode.children = treeData[field.level_1 - 1].children[field.level_2 - 1].children;
            treeData[field.level_1 - 1].children[field.level_2 - 1] = newNode;
        } else {
            newNode.menu_id = treeData[field.level_1 - 1].menu_id;
            newNode.is_system = treeData[field.level_1 - 1].is_system;
            newNode.children = treeData[field.level_1 - 1].children;
            treeData[field.level_1 - 1] = newNode;
        }
        makeNav();
        $('#save').removeClass('layui-btn-disabled').attr('disabled', false);
        layer.msg('修改成功，记得点击左上角的保存按钮');
        return false;
    });

    exports('menu', {})
})