layui.define(['form', 'zadmin', 'layer'], function(exports) {
    var form = layui.form,
        $ = layui.jquery,
        zadmin = layui.zadmin,
        layer = layui.layer,
        menus = {},
        selects = {},
        options = {};

    var loading = zadmin.loading();

    function makeItem(group_id, title='新管理组', rules='') {
        var code = '<tr>';
        code += '<td>';
        if (group_id != 1) code += '<input type="checkbox" name="delete[]" value="' + group_id + '" lay-skin="primary">';
        code += '</td>';
        code += '<td class="t-c"><input type="hidden" name="group_id[]" value="' + group_id + '">' + group_id + '</td>';
        code += '<td><input type="text" name="title[]" required  lay-verify="required" placeholder="不超过16个字" autocomplete="off" class="layui-input" value="' + title + '" maxlength="16"></td>';
        code += '<td><div id="xm_' + group_id + '"></div></td>';
        code += '<td>';
        if (group_id != 1) {
            code += '<div class="layui-btn-group"><button type="button" class="layui-btn layui-btn-sm" data-op="up"><i class="layui-icon layui-icon-up"></i></button><button type="button" class="layui-btn layui-btn-sm" data-op="down"><i class="layui-icon layui-icon-down" ></i></button></div></td>';
        }
        code += '</tr>';
        $('#datalist').append(code);
        options[group_id] = {
            el: '#xm_' + group_id,
            filterable: true,
            name: 'rules[]',
            tree: {
                expandedKeys: [-1],
                show: true,
                showFolderIcon: true,
                showLine: false,
                indent: 20,
                strict: false
            },
            filterable: true,
            height: 'auto',
            data: menus,
            disabled: group_id == 1,
            values: rules.split(','),
            on: function(obj) {
                if (obj.isAdd) {
                    options[group_id].values.push(obj.change[0].value);
                } else {
                    options[group_id].values.splice(options[group_id].values.indexOf(obj.change[0].value));
                }
            },
        };
    }
    $.get(window.location.href, {}, function(res) {
        $('#add').click(function(){
            var maxGroup = 1;
            $('input[name^="group_id"]').each(function(){
                if(parseInt(this.value)>maxGroup){
                    maxGroup = parseInt(this.value);
                }
            })
            makeItem(maxGroup+1);
            formRender();
        })
        function formRender() {
            $('button[data-op]').click(function() {
                var row = $(this.parentNode.parentNode.parentNode);
                if (this.dataset.op == 'up') {
                    var prev = row.prev();
                    if (prev.find('input[name^="group_id"]').val() == 1) return;
                    prev.before(row.prop("outerHTML"));
                    row.remove();
                } else if (this.dataset.op == 'down') {
                    var next = row.next();
                    if (next.length == 0) return;
                    next.after(row.prop("outerHTML"));
                    row.remove();
                }
                formRender();
            })
            form.render();
            for (var x in options) {
                selects[x] = xmSelect.render(options[x])
                selects[x].setValue(options[x].values);
            }
        }

        function doReturn(res) {
            function doMenus(items) {
                return items.map(function(n) {
                    var data = {
                        name: n.title,
                        value: n.menu_id
                    }
                    if (n.children && n.children.length > 0) {
                        data.children = doMenus(n.children);
                    }
                    return data;
                })
            }
            menus = [{ 'name': '全站权限', 'value': -1, 'children': doMenus(res.data.menus) }];
            for (var i = 0; i < res.data.groups.length; i++) {
                makeItem(res.data.groups[i].group_id, res.data.groups[i].title, res.data.groups[i].rules);
            }
            formRender();
        }
        doReturn(res);
        form.on('submit(save)', function(data) {
            var loadingIndex = zadmin.loading();
            $.post(window.location.href, data.field, function(res) {
                layer.close(loadingIndex);
                zadmin.showReturn(res, function(res) {
                    $('#datalist').empty();
                    doReturn(res);
                    formRender();
                })
            })
            return false;
        })
        layer.close(loading);
    })

    exports('groups', {})
})