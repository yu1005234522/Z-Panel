/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost:3306
 Source Schema         : zpanel

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 23/12/2019 00:08:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for field
-- ----------------------------
DROP TABLE IF EXISTS `field`;
CREATE TABLE `field`  (
  `field_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `defaultvalue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `type` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `setting` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tips` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `displayorder` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `available` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `required` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `mods` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`field_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of field
-- ----------------------------
INSERT INTO `field` VALUES (12, '姓名', '', 'text', '', '', 0, 1, 1, '0');
INSERT INTO `field` VALUES (13, '性别', '1', 'radio', '1|男,2|女', '', 1, 1, 1, '0');
INSERT INTO `field` VALUES (14, '出生日期', '', 'date', '', '', 2, 1, 1, '0');
INSERT INTO `field` VALUES (15, '手机号', '', 'text', '', '', 3, 1, 1, '1');
INSERT INTO `field` VALUES (16, '邮箱', '', 'text', '', '', 4, 1, 1, '1');
INSERT INTO `field` VALUES (17, 'QQ', '', 'text', '', '', 5, 1, 0, '1');
INSERT INTO `field` VALUES (18, '微信', '', 'text', '', '', 6, 1, 0, '1');

-- ----------------------------
-- Table structure for group
-- ----------------------------
DROP TABLE IF EXISTS `group`;
CREATE TABLE `group`  (
  `group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `displayorder` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of group
-- ----------------------------
INSERT INTO `group` VALUES (1, '站长', '-1', 0);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `level` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1顶部2侧边3侧边二级',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0菜单1打开窗口3跳转链接4新窗口跳转链接',
  `available` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `is_system` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `url` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `app` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `controller` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `action` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `displayorder` mediumint(6) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, '系统设置', 1, 0, 1, 1, 0, '', 'index', 'setting', '', 0);
INSERT INTO `menu` VALUES (2, '全站设置', 2, 1, 1, 1, 1, '', 'index', 'setting', 'site', 0);
INSERT INTO `menu` VALUES (3, '菜单设置', 2, 1, 1, 1, 1, '', 'index', 'setting', 'menu', 1);
INSERT INTO `menu` VALUES (4, '权限管理', 2, 0, 1, 1, 1, '', 'index', 'user', '', 2);
INSERT INTO `menu` VALUES (5, '资料项设置', 3, 1, 1, 1, 4, '', 'index', 'user', 'field', 0);
INSERT INTO `menu` VALUES (6, '用户组', 3, 1, 1, 1, 4, '', 'index', 'user', 'groups', 2);
INSERT INTO `menu` VALUES (7, '用户列表', 3, 1, 1, 1, 4, '', 'index', 'user', 'list', 1);
INSERT INTO `menu` VALUES (8, '使用手册', 2, 4, 1, 1, 1, 'https://gitee.com/crystar/Z-Panel/wikis/', '', '', '', 3);
INSERT INTO `menu` VALUES (9, 'Z-Panel官网', 1, 4, 1, 1, 0, 'https://z.dqzhiyu.com', '', '', '', 1);

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting`  (
  `setting_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `setkey` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `setvalue` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_system` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `setting` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`setting_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of setting
-- ----------------------------
INSERT INTO `setting` VALUES (1, 'sitename', '浙江德清智羽网络技术有限公司', 1, '面板名称', 'text', '');
INSERT INTO `setting` VALUES (2, 'welcometips', '欢迎使用Z-admin', 1, '回应界面提示语', 'textarea', '');
INSERT INTO `setting` VALUES (3, 'logintoken', '', 1, '登录Token校验', 'switch', '');
INSERT INTO `setting` VALUES (4, 'infomod', '基本资料\n联系方式', 1, '资料页模块（每行一个）', 'textarea', '');
INSERT INTO `setting` VALUES (5, 'modifyemail', '', 1, '允许用户修改邮箱', 'switch', '');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `salt` char(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `avatar` char(52) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `group_id` tinyint(2) UNSIGNED NOT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0禁止登录1允许后台',
  `create_ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `login_ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `login_time` int(11) NULL DEFAULT NULL,
  `last_ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  `addition` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `field12` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `field13` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1',
  `field14` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `field15` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `field16` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `field17` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `field18` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'z@dqzhiyu.com', 'gtMtTJptcLtjuDHe', 'd64c3d752b04cf8cf4ee049680c3753c', '', 1, 1, '127.0.0.1', 1576247769, '127.0.0.1', 1577029873, '127.0.0.1', 1577029873, '127.0.0.1', 1576948475, NULL, NULL, '', '1', '', '', '', '', '');

SET FOREIGN_KEY_CHECKS = 1;
